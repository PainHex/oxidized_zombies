#[macro_use]
extern crate gfx;
extern crate gfx_device_gl;
extern crate ggez;
extern crate specs;
extern crate uuid;
extern crate rayon;
extern crate rand;
extern crate noise;

use ggez::conf::*;
use ggez::event::*;
use ggez::{GameResult, Context};
use ggez::graphics;
use ggez::timer;
use ggez::nalgebra as na;

pub mod game;
pub mod renderer;

use game::game::Game as Game;

struct MainState<'a, 'b> {
    running: bool,
    game: Game<'a, 'b>,
    frames: u64,
}

impl<'a, 'b> MainState<'a, 'b> {
    fn new(ctx: &mut Context) -> GameResult<MainState<'a, 'b>> {
        let s = MainState {
            running: true,
            game: Game::new(ctx),
            frames: 0,
        };
        Ok(s)
    }
}

impl<'a, 'b> EventHandler for MainState<'a, 'b> {
    fn update(&mut self, ctx: &mut Context) -> GameResult<()> {
        if !self.running {
            ctx.quit().ok();
        }

        const DESIRED_FPS: u32 = 60;

        while timer::check_update_time(ctx, DESIRED_FPS) {
            self.game.update();
        }

        self.frames += 1;

        if self.frames % 15 == 0 {
            println!("FPS: {}", timer::get_fps(ctx));
        }

        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        self.game.render(ctx);
        graphics::present(ctx);
        Ok(())
    }

    fn key_down_event(&mut self, _ctx: &mut Context, key_code: Keycode, _key_mod: Mod, _repeat: bool) {
        match key_code {
            Keycode::Escape => self.running = false,
            _ => (),
        }

        self.game.set_key(key_code);
    }

    fn key_up_event(&mut self, _ctx: &mut Context, key_code: Keycode, _key_mod: Mod, _repeat: bool) {
        self.game.un_set_key(key_code);
    }
}

pub fn main() {
    let mut window_mode = WindowMode::default();
    window_mode.width = 1024;
    window_mode.height = 768;
    window_mode.fullscreen_type = FullscreenType::Off;

    let mut window_setup = WindowSetup::default()
        .title("Oxidized Zombies");
    window_setup.resizable = true;
    let c = Conf {
        window_mode,
        window_setup,
        backend: Backend::OpenGL { major: 4, minor: 1 },
    };

    let ctx = &mut Context::load_from_conf("oxidized_zombies", "PainHex", c).unwrap();
    let state = &mut MainState::new(ctx).unwrap();
    run(ctx, state).unwrap();
}