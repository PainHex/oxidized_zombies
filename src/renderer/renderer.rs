extern crate gfx;
extern crate gfx_device_gl;
extern crate ggez;
extern crate image;

use gfx_device_gl::*;
use gfx::Factory;
use gfx::traits::FactoryExt;
use gfx::PipelineState;
use ggez::Context;
use ggez::graphics;
use ggez::nalgebra as na;
use na::{Matrix4, Point2, Point3, Vector3, Vector2};

use std::collections::HashMap;
use std::io::Read;
use std::fs::File;

use renderer::shader_data::pipe::Meta;
use renderer::shader_data::*;
use renderer::mesh::Mesh;
use game::physics::collision::aabb2::AABB2;

pub struct Renderer {
    encoder: gfx::Encoder<Resources, CommandBuffer>,
    factory: gfx_device_gl::Factory,
    color_view: gfx::handle::RenderTargetView<gfx_device_gl::Resources, (gfx::format::R8_G8_B8_A8, gfx::format::Srgb)>,
    shaders: HashMap<String, PipelineState<Resources, Meta>>,
    textures: HashMap<String, gfx::handle::ShaderResourceView<Resources, [f32; 4]>>,
    projection: Matrix4<f32>,
    view: Matrix4<f32>,
    view_space: AABB2,
    width: f32,
    height: f32,
    current_shader: String,
    current_texture: String,
}

impl Renderer {
    pub fn new(ctx: &mut Context) -> Self {
        let mut factory = graphics::get_factory(ctx).clone();
        Renderer {
            encoder: factory.create_command_buffer().into(),
            factory,
            color_view: graphics::get_screen_render_target(ctx).clone(),
            shaders: HashMap::new(),
            textures: HashMap::new(),
            projection: na::Orthographic3::new(0.0, 1024.0, 0.0, 768.0, 0.0, 2.0).to_homogeneous(),
            view: Matrix4::new_scaling(1.0),
            view_space: AABB2::new(Point2::new(0.0, 0.0), Point2::new(1024.0, 768.0)),
            width: 1024.0,
            height: 768.0,
            current_shader: "".to_string(),
            current_texture: "".to_string(),
        }
    }

    pub fn add_shader(&mut self, name: String) {
        let pso = self.factory.create_pipeline_simple(
            &read_file("assets/shader/".to_string() + name.as_str() + "/vertex.glsl"),
            &read_file("assets/shader/".to_string() + name.as_str() + "/frag.glsl"),
            pipe::new(),
        ).unwrap();

        self.shaders.insert(name.clone(), pso);
        self.current_shader = name;
    }

    pub fn load_texture(&mut self, name: String) {
        let texture = load_texture(&mut self.factory, &("assets/textures/".to_string() + name.as_str() + ".png"));
        self.textures.insert(name.clone(), texture);
        self.current_texture = name;
    }

    pub fn set_shader(&mut self, name: String) {
        self.current_shader = name;
    }

    pub fn set_texture(&mut self, name: String) {
        self.current_texture = name;
    }

    pub fn paint(&mut self, mut mesh: &mut Mesh) {
        self.paint_mesh(&mut mesh);
    }

    pub fn paint_mesh(&mut self, mesh: &mut Mesh) {
        if !self.can_cull(mesh) {
            let mut factory = self.factory.clone();
            mesh.update_buffer(&mut factory);

            let (vertex_buffer, slice) = mesh.buffer_data();

            let sampler = factory.create_sampler_linear();

            let data = pipe::Data {
                projection: self.projection.into(),
                view: self.view.into(),
                vbuf: vertex_buffer,
                texture: (self.textures.get(self.current_texture.as_str()).unwrap().clone(), sampler),
                out: self.color_view.clone(),
            };

            let shader = self.get_shader(&self.current_shader);
            self.encoder.draw(&slice, &shader, &data);
        }
    }

    pub fn set_view(&mut self, center: Point2<f32>) {
        let (half_x, half_y) = (self.width * 0.5, self.height * 0.5);
        let eye = Point3::new(center.x - half_x, center.y - half_y, 1.0);
        let target = Point3::new(center.x - half_x, center.y - half_y, 0.0);
        self.view_space = AABB2::new(center - Vector2::new(self.width * 0.5, self.height * 0.5), center + Vector2::new(self.width * 0.5, self.height * 0.5));
        self.view = na::Isometry3::look_at_rh(&eye, &target, &Vector3::y()).to_homogeneous();
    }

    pub fn flush(&mut self, device: &mut Device) {
        self.encoder.flush(device);
    }

    pub fn clear(&mut self) {
        self.encoder.clear(&self.color_view.clone(), [0.1, 0.1, 0.1, 1.0].into());
    }

    pub fn borrow_factory(&self) -> &gfx_device_gl::Factory {
        &self.factory
    }

    fn get_shader(&self, name: &String) -> PipelineState<Resources, Meta> {
        self.shaders.get(name).unwrap().clone()
    }

    fn can_cull(&self, mesh: &Mesh) -> bool {
        for ref vertex in &mesh.get_vertex_data().0 {
            let point = Point2::new(vertex.pos[0], vertex.pos[1]);
            if self.view_space.intersects(&AABB2::new(point, point)) {
                return false;
            }
        }
        true
    }
}

fn load_texture(factory: &mut gfx_device_gl::Factory, path: &str) -> gfx::handle::ShaderResourceView<Resources, [f32; 4]> {
    let img = image::open(path).unwrap().to_rgba();
    let (width, height) = img.dimensions();
    let kind = gfx::texture::Kind::D2(width as u16, height as u16, gfx::texture::AaMode::Single);
    let (_, view) = factory.create_texture_immutable_u8::<ColorFormat>(kind, &[&img]).unwrap();
    view
}

fn read_file(path: String) -> Vec<u8> {
    let mut buffer = String::new();
    let mut f = File::open(path).ok().unwrap();
    f.read_to_string(&mut buffer).ok();
    buffer.into_bytes()
}
