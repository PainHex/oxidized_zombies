use gfx::Factory;
use gfx::traits::FactoryExt;
use gfx_device_gl::{Resources};
use gfx_device_gl;
use gfx::Slice;
use gfx::handle::Buffer;

use renderer::shader_data::*;
use na::{Vector2, Point2};

pub enum MeshType {
    Circle,
    Square,
    Triangle,
    Complex,
}

pub struct Mesh {
    mesh_type: MeshType,
    vbo_data: (Vec<Vertex>, Vec<u32>),
    buffer_data: Option<(Buffer<Resources, Vertex>, Slice<Resources>)>,
    mesh_dirty: bool,
}

impl Mesh {
    pub fn new(mesh_type: MeshType) -> Mesh {
        Mesh {
            mesh_type,
            vbo_data: (vec![], vec![]),
            buffer_data: None,
            mesh_dirty: true,
        }
    }

    pub fn square(centre: Point2<f32>, half_size: Vector2<f32>) -> Mesh {
        Mesh {
            mesh_type: MeshType::Complex,
            vbo_data: get_square_vertex_data(centre, half_size),
            buffer_data: None,
            mesh_dirty: true,
        }
    }

    pub fn get_vertex_data(&self) -> &(Vec<Vertex>, Vec<u32>) {
        &self.vbo_data
    }

    pub fn set_vbo_data(&mut self, vbo_data: (Vec<Vertex>, Vec<u32>)) {
        self.vbo_data = vbo_data;
        self.mesh_dirty = true;
    }

    pub fn mesh_type(&self) -> &MeshType {
        &self.mesh_type
    }

    pub fn update_buffer(&mut self, factory: &mut gfx_device_gl::Factory) {
        if self.mesh_dirty {
            self.buffer_data = Some(factory.create_vertex_buffer_with_slice(&self.vbo_data.0, &*self.vbo_data.1));
            self.mesh_dirty = false;
        }
    }

    pub fn buffer_data(&self) -> (Buffer<Resources, Vertex>, Slice<Resources>) {
        self.buffer_data.as_ref().unwrap().clone()
    }

}

fn get_square_vertex_data(center: Point2<f32>, half_size: Vector2<f32>) -> (Vec<Vertex>, Vec<u32>) {
    let (mut vertices, mut indices) = (vec![], vec![]);

    let (x_pos, y_pos) = (center.x, center.y);

    vertices.extend(&[
        Vertex { pos: [x_pos + half_size.x, y_pos - half_size.y], uv: [1.0, 1.0], color: WHITE },
        Vertex { pos: [x_pos - half_size.x, y_pos - half_size.y], uv: [0.0, 1.0], color: WHITE },
        Vertex { pos: [x_pos - half_size.x, y_pos + half_size.y], uv: [0.0, 0.0], color: WHITE },
        Vertex { pos: [x_pos + half_size.x, y_pos + half_size.y], uv: [1.0, 0.0], color: WHITE },
    ]);

    indices.extend(&[
        0, 1, 2, 2, 3, 0
    ]);

    (vertices, indices)
}