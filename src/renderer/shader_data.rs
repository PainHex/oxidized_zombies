extern crate gfx;

pub type ColorFormat = self::gfx::format::Srgba8;
pub type DepthFormat = self::gfx::format::DepthStencil;

pub const WHITE: [f32; 3] = [1.0, 1.0, 1.0];

gfx_defines! {
    vertex Vertex {
        pos: [f32; 2] = "pos",
        uv: [f32; 2] = "uv",
        color: [f32; 3] = "color",
    }

    pipeline pipe {
        vbuf: gfx::VertexBuffer<Vertex> = (),
        texture: gfx::TextureSampler<[f32; 4]> = "t_texture",
        projection: gfx::Global<[[f32; 4]; 4]> = "projection",
        view: gfx::Global<[[f32; 4]; 4]> = "view",
        out: gfx::RenderTarget<ColorFormat> = "Target0",
    }
}