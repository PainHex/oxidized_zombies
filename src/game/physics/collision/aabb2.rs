use ggez::nalgebra as na;
use ggez::nalgebra::{Vector2, Point2};

#[derive(Clone)]
pub struct AABB2 {
    min: Point2<f32>,
    max: Point2<f32>
}

impl AABB2 {
    pub fn new(min: Point2<f32>, max: Point2<f32>) -> Self {
        Self {
            min,
            max
        }
    }

    pub fn intersects(&self, other: &AABB2) -> bool {
        na::partial_le(&self.min, &other.max) && na::partial_ge(&self.max, &other.min)
    }

    pub fn contains(&self, other: &AABB2) -> bool {
        na::partial_le(&self.min, &other.min) && na::partial_ge(&self.max, &other.max)
    }
}