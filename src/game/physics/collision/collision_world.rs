use std::collections::HashMap;

use game::physics::collision::aabb2;

#[derive(Clone)]
pub struct ShapeHandle {
    id: u32,
    bounding_volume: aabb2::AABB2,
}

impl ShapeHandle {

    pub fn new(id: u32, bounding_volume: aabb2::AABB2) -> Self {
        Self {
            id,
            bounding_volume
        }
    }

    pub fn bounding_volume(&self) -> &aabb2::AABB2 {
        &self.bounding_volume
    }

}

pub struct CollisionWorld {

    collision_shapes: HashMap<u32, ShapeHandle>,
    last_id: u32,

}

impl CollisionWorld {

    pub fn new() -> Self {
        Self {
            collision_shapes: HashMap::new(),
            last_id: 0
        }
    }

    pub fn add_shape(&mut self, bounding_volume: aabb2::AABB2) -> Option<ShapeHandle> {
        let shape_handle = ShapeHandle {id: self.last_id, bounding_volume};
        self.last_id = self.last_id + 1;
        self.collision_shapes.insert(shape_handle.id.clone(), shape_handle)
    }

    pub fn check_collisions(&self, collision_volume: aabb2::AABB2) -> Vec<ShapeHandle> {
        self.collision_shapes.iter().clone()
            .filter(|(id, shape_handle)| collision_volume.intersects(&shape_handle.bounding_volume()))
            .map(|(id, shape_handle)| shape_handle.clone())
            .collect::<Vec<ShapeHandle>>()
    }
}