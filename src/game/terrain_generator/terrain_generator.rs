use noise::{Seedable, NoiseFn, Fbm, ScalePoint, MultiFractal, RidgedMulti};

use game::components::chunk::Block;

pub struct TerrainGenerator {
    height_generator: Fbm,
    cave_generator: RidgedMulti,
}

impl TerrainGenerator {

    pub fn new() -> Self {
        let height_generator = Fbm::new().set_seed(50)
            .set_octaves(16)
            .set_frequency(0.2)
            .set_lacunarity(2.8231);

        let cave_generator = RidgedMulti::new().set_seed(89)
            .set_attenuation(1.2);

        Self {
            height_generator,
            cave_generator,
        }
    }

    pub fn block_at(&self, x: f32, y: f32) -> Block {
        let height_sample = ScalePoint::new(&self.height_generator).set_x_scale(0.0005).get([x as f64, 0.0]) * 1000.0;
        let cave_sample = self.cave_generator.get([x as f64 / 1000.0 as f64, y as f64 / 1000.0 as f64]);

        if (y as f64 > height_sample) || cave_sample < -0.7 {
            Block::new(false)
        } else {
            Block::new(true)
        }
    }

}