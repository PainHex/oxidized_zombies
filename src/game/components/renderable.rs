use specs::*;

use renderer::mesh::Mesh;
use renderer::renderer::Renderer;

use game::components::player::Player;
use game::components::position::Position;

pub struct Renderable {
    pub shader: String,
    pub texture: String,
    pub mesh: Mesh,
}

impl Component for Renderable {
    type Storage = VecStorage<Self>;
}

pub struct RenderingSystem<'a> {
    renderer: &'a mut Renderer,
}

impl<'a> RenderingSystem<'a> {
    pub fn new(renderer: &'a mut Renderer) -> RenderingSystem<'a> {
        Self {
            renderer
        }
    }
}

impl<'a> System<'a> for RenderingSystem<'a> {
    type SystemData = (Entities<'a>, WriteStorage<'a, Renderable>, ReadStorage<'a, Position>, ReadStorage<'a, Player>);

    fn run(&mut self, (entities, mut renderables, positions, players): Self::SystemData) {
        self.renderer.clear();

        for (_entity, position, _player) in (&*entities, &positions, &players).join() {
            self.renderer.set_view(position.pos);
        }

        for (_entity, mut renderable) in (&*entities, &mut renderables).join() {
            self.renderer.set_shader(renderable.shader.clone());
            self.renderer.set_texture(renderable.texture.clone());
            self.renderer.paint(&mut renderable.mesh);
        }
    }
}