use ggez::nalgebra::{Point2, Vector2};

use specs::*;

use noise::{Seedable, NoiseFn, Fbm, ScalePoint, MultiFractal, RidgedMulti};

use std::collections::HashMap;

use renderer::shader_data::*;
use renderer::mesh::{Mesh, MeshType};
use game::components::renderable::Renderable;
use game::components::position::Position;
use game::components::player::Player;
use game::terrain_generator::terrain_generator::TerrainGenerator;

use game::components::debug;

const BLOCK_HEIGHT: f32 = 32.0;
const BLOCK_WIDTH: f32 = 32.0;
const CHUNK_SIZE_X: u32 = 16;
const CHUNK_SIZE_Y: u32 = 16;
const CHUNK_WIDTH: f32 = BLOCK_WIDTH * CHUNK_SIZE_X as f32;
const CHUNK_HEIGHT: f32 = BLOCK_HEIGHT * CHUNK_SIZE_Y as f32;

#[derive(Clone)]
pub struct Block {
    active: bool,
}

#[derive(Clone)]
pub struct Chunk {
    position: Point2<f32>,
    blocks: Vec<Block>,
}

impl Block {
    pub fn new(active: bool) -> Block {
        Block {
            active
        }
    }

    pub fn is_active(&self) -> bool {
        self.active
    }
}

impl Component for Block {
    type Storage = VecStorage<Self>;
}

impl Chunk {
    pub fn new(position: Point2<f32>, terrain_generator: &TerrainGenerator) -> Chunk {
        let mut blocks = vec![];

        Chunk {
            position,
            blocks,
        }
    }

    pub fn set_position(&mut self, position: Point2<f32>) {
        self.position = position;
    }

    fn get_vertex_data(&self) -> (Vec<Vertex>, Vec<u32>) {
        let (mut vertices, mut indices) = (vec![], vec![]);

        let (half_x, half_y) = (BLOCK_WIDTH * 0.5, BLOCK_HEIGHT * 0.5);
        let mut i = 0 as u32;
        for x_index in 0..CHUNK_SIZE_X {
            for y_index in 0..CHUNK_SIZE_Y {
                let (x_pos, y_pos) = (x_index as f32 * BLOCK_WIDTH + self.position.x, y_index as f32 * BLOCK_HEIGHT + self.position.y);

                let corresponding_block = self.blocks.get(((x_index * CHUNK_SIZE_X) + y_index) as usize).unwrap();

                if corresponding_block.is_active() {
                    vertices.extend(&[
                        Vertex { pos: [x_pos + half_x, y_pos - half_y], uv: [1.0, 1.0], color: WHITE },
                        Vertex { pos: [x_pos - half_x, y_pos - half_y], uv: [0.0, 1.0], color: WHITE },
                        Vertex { pos: [x_pos - half_x, y_pos + half_y], uv: [0.0, 0.0], color: WHITE },
                        Vertex { pos: [x_pos + half_x, y_pos + half_y], uv: [1.0, 0.0], color: WHITE },
                    ]);
                    indices.extend(&[
                        4 * i, 4 * i + 1, 4 * i + 2, 4 * i + 2, 4 * i + 3, 4 * i
                    ]);
                    i += 1;
                }
            }
        }

        (vertices, indices)
    }
}

impl Component for Chunk {
    type Storage = VecStorage<Self>;
}

pub struct TerrainSystem {
    chunks: HashMap<Point2<i32>, Chunk>,
    terrain_generator: TerrainGenerator,
}

impl TerrainSystem {
    pub fn new() -> TerrainSystem {
        TerrainSystem {
            chunks: HashMap::new(),
            terrain_generator: TerrainGenerator::new(),
        }
    }
}

impl<'a> System<'a> for TerrainSystem {
    type SystemData = (Entities<'a>, WriteStorage<'a, Chunk>, WriteStorage<'a, Renderable>, ReadStorage<'a, Position>, ReadStorage<'a, Player>, Fetch<'a, LazyUpdate>);

    fn run(&mut self, (entities, mut chunks, renderables, positions, players, updater): Self::SystemData) {
        let mut player_position = Point2::new(0.0, 0.0);
        for (_entity, position, _player) in (&*entities, &positions, &players).join() {
            player_position = position.pos;
        }

        let (x_index_start, y_index_start) = ((player_position.x / CHUNK_WIDTH) as i32 - 2, (player_position.y / CHUNK_WIDTH) as i32 - 2);

        for x in x_index_start..x_index_start + 4 {
            for y in y_index_start..y_index_start + 4 {
                if !self.chunks.contains_key(&Point2::new(x, y)) {
                    let chunk_pos = Point2::new(x as f32 * CHUNK_WIDTH, y as f32 * CHUNK_HEIGHT);
                    let chunk = Chunk::new(chunk_pos, &self.terrain_generator);

                    for x_index in 0..CHUNK_SIZE_X {
                        for y_index in 0..CHUNK_SIZE_Y {
                            let x_pos = chunk_pos.x + (x_index as f32 * BLOCK_WIDTH);
                            let y_pos = chunk_pos.y + (y_index as f32 * BLOCK_HEIGHT);

                            if self.terrain_generator.block_at(x_pos, y_pos).is_active() {
                                let block = entities.create();

                                let position = Point2::new(x_pos, y_pos);
                                let size = Vector2::new(BLOCK_WIDTH * 0.5, BLOCK_HEIGHT * 0.5);

                                updater.insert(
                                    block,
                                    Renderable { shader: "basic".to_string(), texture: "terrain/dirt".to_string(), mesh: Mesh::square(position, size) },
                                );

                                updater.insert(
                                    block,
                                    Position::new(position),
                                );
                            }
                        }
                    }

                    self.chunks.insert(Point2::new(x, y), chunk);
                }
            }
        }

//        for (_entity, block, renderable) in (&*entities, &mut chunks, &mut renderables).join() {
//            renderable.mesh.set_vbo_data(chunk.get_vertex_data());
//        }

        //println!("Total loaded chunks: {}", self.chunks.len());
    }
}