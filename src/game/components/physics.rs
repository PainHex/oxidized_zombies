use specs::*;

use na::{Vector2, Point2, Translation2, Translation, zero};

use uuid::*;

use std::collections::HashMap;
use std::rc::Rc;

use game::components::position::Position;

pub struct PhysicsSystem {}

impl<'a> System<'a> for PhysicsSystem {
    type SystemData = (Entities<'a>, WriteStorage<'a, Position>);

    fn run(&mut self, (entities, mut positions): Self::SystemData) {}
}