use specs::*;
use na::Point2;

pub struct Position {
    pub pos: Point2<f32>,
}

impl Position {
    pub fn new(position: Point2<f32>) -> Self {
        Self {
            pos: position,
        }
    }
}

impl Component for Position {
    type Storage = VecStorage<Self>;
}