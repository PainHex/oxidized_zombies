use specs::*;

use na::{Vector2};

use game::components::position::Position;
use game::components::renderable::Renderable;
use renderer::mesh::Mesh;

const PLAYER_HEIGHT_HALF: f32 = 20.0;
const PLAYER_WIDTH_HALF: f32 = 12.0;

pub struct Player();

impl Component for Player {
    type Storage = VecStorage<Self>;
}

pub struct PlayerSystem();
impl<'a> System<'a> for PlayerSystem {
    type SystemData = (Entities<'a>, WriteStorage<'a, Renderable>, ReadStorage<'a, Position>, ReadStorage<'a, Player>);

    fn run(&mut self, (entities, mut renderables, positions, players): Self::SystemData) {

        for (_entity, renderable, position, _player) in (&*entities, &mut renderables, &positions, &players).join() {
            renderable.mesh = Mesh::square(position.pos, Vector2::new(PLAYER_WIDTH_HALF, PLAYER_HEIGHT_HALF));
        }

    }
}