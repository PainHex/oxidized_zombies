use std::collections::HashSet;
use ggez::event::Keycode;

use na::{Vector2};
use specs::*;

pub struct Keyboard {
    keys: HashSet<Keycode>,
}

impl Keyboard {

    pub fn new() -> Keyboard {
        Keyboard {
            keys: HashSet::new(),
        }
    }

    pub fn is_key_pressed(&self, key: Keycode) -> bool {
        self.keys.contains(&key)
    }

    pub fn set_key(&mut self, key: Keycode) {
        self.keys.insert(key);
    }

    pub fn un_set_key(&mut self, key: Keycode) {
        self.keys.remove(&key);
    }
}

pub struct Controllable {
    pub direction: Vector2<f32>,
}

impl Controllable {

    pub fn new() -> Controllable {
        Controllable {
            direction: Vector2::new(0.0, 0.0)
        }
    }

    pub fn set_direction(&mut self, direction: Vector2<f32>) {
        self.direction = direction;
    }

}

impl Component for Controllable {
    type Storage = HashMapStorage<Self>;
}

pub struct Controller;
impl<'a> System<'a> for Controller {
    type SystemData = (
        Fetch<'a, Keyboard>,
        WriteStorage<'a, Controllable>,
    );

    fn run(&mut self, (keyboard, mut controllables): Self::SystemData) {

    }
}
