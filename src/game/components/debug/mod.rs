extern crate image;

use noise::NoiseFn;
use std;
use std::path::Path;

#[inline]
pub fn clamp<T: PartialOrd>(input: T, min: T, max: T) -> T {
    assert!(max >= min);

    let mut x = input;
    if x < min {
        x = min;
    }
    if x > max {
        x = max;
    }
    x
}

#[allow(dead_code)]
pub fn render_noise_module2<M>(filename: &str, module: &M, width: u32, height: u32, zoom: u32)
    where
        M: NoiseFn<[f64; 2]>,
{
    let mut pixels = Vec::with_capacity((width * height) as usize);

    println!("\nGenerating {} points for {}", width * height, filename);
    let mut min_value = std::f64::MAX;
    let mut max_value = std::f64::MIN;

    for y in 0..height {
        for x in 0..width {
            let value = module.get(
                [
                    ((x as f64 - (width as f64 / 2.0)) / zoom as f64),
                    ((y as f64 - (height as f64 / 2.0)) / zoom as f64),
                ],
            );
            pixels.push((clamp(value * 0.5 + 0.5, 0.0, 1.0) * 255.0) as u8);

            print!("\rProcessing {} of {}",
                   (y * width) + height,
                   width * height);

            if value > max_value {
                max_value = value
            };
            if value < min_value {
                min_value = value
            };
        }
    }

    let _ = image::save_buffer(&Path::new(filename),
                               &*pixels,
                               width,
                               height,
                               image::Gray(8));

    println!("\nFinished generating {}", filename);
    println!("\nMaxValue: {}", max_value);
    println!("\nMinValue: {}", min_value);
}