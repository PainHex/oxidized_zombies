use na::{Vector2, Point2};
use ggez::event::Keycode;
use ggez::Context;
use ggez::graphics;

use specs::{World, DispatcherBuilder, Dispatcher, RunNow};

use renderer::renderer::Renderer;
use renderer::mesh::{Mesh, MeshType};
use game::components::*;

pub struct Game<'a, 'b> {
    world: World,
    renderer: Renderer,
    dispatcher: Dispatcher<'a, 'b>,
}

impl<'a, 'b> Game<'a, 'b> {
    pub fn new(ctx: &mut Context) -> Game<'a, 'b> {
        let mut renderer = Renderer::new(ctx);
        renderer.add_shader("basic".to_string());
        renderer.load_texture("mario".to_string());
        renderer.load_texture("terrain/dirt".to_string());

        let mut world = World::new();
        world.register::<renderable::Renderable>();
        world.register::<player::Player>();
        world.register::<position::Position>();
        world.register::<position::Position>();
        world.register::<controllable::Controllable>();
        world.register::<chunk::Chunk>();

        world.add_resource(controllable::Keyboard::new());

        world.create_entity()
            .with(player::Player())
            .with(position::Position { pos: Point2::new(0.0, 200.0) })
            .with(controllable::Controllable::new())
            .with(renderable::Renderable { shader: "basic".to_string(), texture: "mario".to_string(), mesh: Mesh::new(MeshType::Complex) })
            .build();


        let dispatcher = DispatcherBuilder::new()
            .add(controllable::Controller, "controller", &[])
            .add(physics::PhysicsSystem {}, "physics", &[])
            .add(chunk::TerrainSystem::new(), "terrain", &[])
            .add(player::PlayerSystem {}, "player", &[])
            .build();

        Game {
            world,
            renderer,
            dispatcher,
        }
    }

    pub fn render(&mut self, ctx: &mut Context) {
        {
            let mut rendering_system = renderable::RenderingSystem::new(&mut self.renderer);
            rendering_system.run_now(&mut self.world.res);
        }

        self.renderer.flush(graphics::get_device(ctx));
    }

    pub fn update(&mut self) {
        self.dispatcher.dispatch(&mut self.world.res);
        self.world.maintain();
    }

    pub fn set_key(&mut self, key: Keycode) {
        let mut keyboard = self.world.write_resource::<controllable::Keyboard>();
        keyboard.set_key(key);
    }

    pub fn un_set_key(&mut self, key: Keycode) {
        let mut keyboard = self.world.write_resource::<controllable::Keyboard>();
        keyboard.un_set_key(key);
    }
}