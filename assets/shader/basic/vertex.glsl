#version 150

uniform mat4 projection;
uniform mat4 view;

in vec2 pos;
in vec2 uv;
in vec3 color;

out vec4 out_color;
out vec2 v_uv;

void main() {
    v_uv = uv;
    out_color = vec4(1.0, 1.0, 1.0, 1.0);
    gl_Position = projection * view * vec4(pos, 0.0, 1.0);
}