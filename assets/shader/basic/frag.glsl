#version 150 core

uniform sampler2D t_texture;

in vec4 out_color;
in vec2 v_uv;
out vec4 Target0;

void main() {
    vec4 uv_map = texture(t_texture, v_uv).rgba;

    if (vec3(uv_map) == vec3(0.0, 0.0, 0.0)) {
        Target0 = out_color;
    } else {
        Target0 = uv_map;
    }
}